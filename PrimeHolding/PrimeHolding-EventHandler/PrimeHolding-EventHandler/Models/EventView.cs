﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrimeHolding_EventHandler.Models
{
    public class EventView
    {
        public  int Id { get; set; }
        public  String Name { get; set; }
        public  String Location { get; set; }
        public  DateTime StartDate { get; set; }
        public  DateTime EndDate { get; set; }

        public EventView() { }

        public EventView(int id, String name, String location, DateTime startDate, DateTime endDate)
        {
            Id = id;
            Name = name;
            Location = location;
            StartDate = startDate;
            EndDate = endDate;
        }

        public EventView(Event eventTmp)
        {
            Id = eventTmp.Id;
            Name = eventTmp.Name;
            Location = eventTmp.Location;
            StartDate = eventTmp.StartDate;
            EndDate = eventTmp.EndDate;
        }
    }
}