﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrimeHolding_EventHandler.Models
{
    public class Event
    {
        public virtual int Id { get; set; }
        public virtual String Name { get; set; }
        public virtual String Location { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }

        //public Event(int id, String name, String location, DateTime startDate, DateTime endDate)
        //{
        //    Id = id;
        //    Name = name;
        //    Location = location;
        //    StartDate = startDate;
        //    EndDate = endDate;
        //}
    }
}