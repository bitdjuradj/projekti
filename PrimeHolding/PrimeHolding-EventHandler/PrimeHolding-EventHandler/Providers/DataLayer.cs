﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using PrimeHolding_EventHandler.Mapping;
using NHibernate;

namespace PrimeHolding_EventHandler.Providers
{
    public class DataLayer
    {
        private static ISessionFactory _factory = null;
        private static object objLock = new object();


        //funkcija na zahtev otvara sesiju
        public static ISession GetSession()
        {
            //ukoliko session factory nije kreiran
            if (_factory == null)
            {
                lock (objLock)
                {
                    if (_factory == null)
                        _factory = CreateSessionFactory();
                }
            }

            return _factory.OpenSession();
        }

        //konfiguracija i kreiranje session factory
        private static ISessionFactory CreateSessionFactory()
        {
            try
            {
                var cfg = MySQLConfiguration.Standard.ConnectionString(x => x.Is("server =localhost; Port=3306; uid =root; pwd =; database =event_db"));

                return Fluently.Configure()
                .Database(cfg)
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<EventMapping>())
                .BuildSessionFactory();

            }
            catch (Exception ec)
            {
                return null;
            }

        }
    }
}