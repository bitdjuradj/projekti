﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PrimeHolding_EventHandler.Models;
using NHibernate;
using NHibernate.Linq;

namespace PrimeHolding_EventHandler.Providers
{
    public class EventProvider
    {
        public static List<Event> Get()
        {
            ISession session = DataLayer.GetSession();

            return session.Query<Event>().Select(item => new Event
            {
                Id = item.Id,
                Name = item.Name,
                Location = item.Location,
                StartDate = item.StartDate,
                EndDate = item.EndDate
            }).ToList();
        }

        public static Event Get(int id)
        {
            try
            {
                ISession session = DataLayer.GetSession();

                Event eventTmp = session.Load<Event>(id);

                EventView eventView = new EventView(eventTmp.Id, eventTmp.Name, eventTmp.Location, eventTmp.StartDate, eventTmp.EndDate);

                session.Close();

                return eventTmp;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public static int Post(Event eventTmp)
        {
            try
            {
                ISession session = DataLayer.GetSession();

                session.Save(eventTmp);
                session.Flush();
                session.Close();
            }
            catch (Exception e)
            {
                return -1;
            }
            return 1;
        }

        public static int Put(Event eventTmp)
        {
            try
            {
                ISession session = DataLayer.GetSession();

                session.SaveOrUpdate(eventTmp);
                session.Flush();
                session.Close();
            }
            catch(Exception e)
            {
                return -1;
            }

            return 1;
        }

        public static int Delete(int id)
        {
            try
            {
                ISession session = DataLayer.GetSession();

                //Player player = session.Load<Player>(id);

                Event eventTmp = new Event();
                eventTmp.Id = id;

                session.Delete(eventTmp);
                session.Flush();
                session.Close();
            }
            catch (Exception e)
            {
                return -1;
            }

            return 1;
        }

    }
}