﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;
using PrimeHolding_EventHandler.Models;
using NHibernate;

namespace PrimeHolding_EventHandler.Mapping
{
    public class EventMapping: ClassMap<Event>
    {
        public EventMapping()
        {
            Table("EventInformation");

            Id(x => x.Id, "id").GeneratedBy.Assigned();

            Map(x => x.Name, "eventName");
            Map(x => x.Location, "location");
            Map(x => x.StartDate, "startDate");
            Map(x => x.EndDate, "endDate");
        }
    }
}