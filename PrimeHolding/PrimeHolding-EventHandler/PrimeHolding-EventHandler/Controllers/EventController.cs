﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PrimeHolding_EventHandler.Models;
using PrimeHolding_EventHandler.Providers;

namespace PrimeHolding_EventHandler.Controllers
{
    public class EventController : ApiController
    {
        // GET: api/Event
        public List<Event> Get()
        {
            List <Event> list = EventProvider.Get();
            return list;
        }

        // GET: api/Event/5
        public EventView Get(int id)
        { 
            return new EventView(EventProvider.Get(id));
        }

        // POST: api/Event
        public int Post([FromBody]Event eventTmp)
        {
            return EventProvider.Post(eventTmp);
        }

        // PUT: api/Event/5
        public int Put(int id, [FromBody]Event eventTmp)
        {
            eventTmp.Id = id;
            return EventProvider.Put(eventTmp);
        }

        // DELETE: api/Event/5
        public int Delete(int id)
        {
            return EventProvider.Delete(id);
        }
    }
}
